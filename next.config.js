/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  images: {
    domains: ['cdn.vox-cdn.com'],
    unoptimized: true, // Disable Image Optimization
  },
};

module.exports = {
  images: {
    loader: 'akamai',
    path: '',
  },
}



